import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/actions/init.dart';
import 'package:flutter_list/middleware/logging_middleware.dart';
import 'package:flutter_list/middleware/persistence_middleware.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/reducers/app_reducer.dart';
import 'package:flutter_list/selectors/todos_selector.dart';
import 'package:redux/redux.dart';

// Poor Man's Dependency Injection
class AppModule {
  static final AppModule _singleton = new AppModule._internal();
  final Store<AppState, AppAction> store = new Store<AppState, AppAction>(
      new AppReducer(),
      middleware: [new PersistenceMiddleware(), new LoggingMiddleware()],
      initialState: new AppState.of([]))..dispatch(new Init());

  factory AppModule() {
    return _singleton;
  }

  AppModule._internal();

  TodosSelector get todosSelector {
    return new TodosSelector(store: store);
  }
}
