library flutter_list_model;

import 'package:built_value/built_value.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/src/widgets/editable_text.dart';
import 'package:flutter_list/models/todo.dart';
import 'package:flutter_list/ui/todo_form.dart';

part 'flutter_list_model.g.dart';

abstract class FlutterListModel implements Built<FlutterListModel, FlutterListModelBuilder> {
  Iterable<Todo> get todos;
  TodoFormModel get todoFormModel;
  bool get hasCompleted;
  GestureTapCallback get onClearCompletedTapped;
  TextEditingController get textEditingController;

  FlutterListModel._();

  factory FlutterListModel([updates(FlutterListModelBuilder b)]) = _$FlutterListModel;
}
