import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/actions/remove_todo.dart';
import 'package:flutter_list/actions/toggle_todo_complete.dart';
import 'package:flutter_list/actions/toggle_todo_favorite.dart';
import 'package:flutter_list/actions/undo_remove.dart';
import 'package:flutter_list/injection/app_module.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/models/todo.dart';
import 'package:flutter_list/ui/colors.dart';
import 'package:redux/redux.dart';

class TodoWidget extends StatelessWidget {
  final Todo todo;
  final Store<AppState, AppAction> store;

  TodoWidget(Todo todo, {Store<AppState, AppAction> store})
      : this.todo = todo,
        this.store = store ?? new AppModule().store,
        super(key: new Key(todo.id));

  @override
  Widget build(BuildContext context) {
    return new Dismissible(
        onDismissed: (direction) {
          store.dispatch(new RemoveTodo((b) => b..id = todo.id));
          Scaffold.of(context).showSnackBar(new SnackBar(
                action: new SnackBarAction(
                    label: "Undo",
                    onPressed: () {
                      store.dispatch(new UndoRemoveTodo(
                          (b) => b..todo = todo.toBuilder()));
                    }),
                content: new Text('Todo deleted',
                    // ignore: conflicting_dart_import
                    style: new TextStyle(fontSize: 16.0, fontFamily: 'Lato')),
              ));
        },
        key: new Key(todo.id),
        direction: DismissDirection.horizontal,
        child: new Container(
            decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(3.0),
                color: new Color(0xEEFFFFFF)),
            margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
            height: 60.0,
            child: new Row(children: <Widget>[
              // Checkbox
              new Material(
                  color: Colors.transparent,
                  child: new Container(
                      margin: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                      width: 20.0,
                      height: 20.0,
                      child: new InkWell(
                          onTap: () {
                            store.dispatch(new ToggleTodoComplete((b) => b
                              ..id = todo.id
                              ..isComplete = !todo.isComplete));
                          },
                          child: new Container(
                              padding: new EdgeInsets.all(0.0),
                              decoration: new BoxDecoration(
                                  border: new Border.all(color: lightGray)),
                              child: new AnimatedOpacity(
                                  duration: new Duration(milliseconds: 150),
                                  opacity: todo.isComplete ? 1.0 : 0.0,
                                  child: new Icon(Icons.done,
                                      size: 18.0, color: darkGray)))))),
              // Task Text
              new Expanded(
                  child: new Text(todo.task,
                      style:
                          new TextStyle(fontSize: 16.0, fontFamily: 'Lato'))),

              // Star
              new Material(
                  color: Colors.transparent,
                  child: new InkWell(
                      onTap: () {
                        store.dispatch(new ToggleTodoFavorite((b) => b
                          ..id = todo.id
                          ..isFavorite = !todo.isFavorite));
                      },
                      child: new Container(
                          width: 44.0,
                          padding: new EdgeInsets.fromLTRB(12.0, 0.0, 0.0, 0.0),
                          child: new Stack(children: [
                            new AnimatedOpacity(
                                duration: new Duration(milliseconds: 150),
                                opacity: todo.isFavorite ? 1.0 : 0.0,
                                child: new Container(
                                    alignment: FractionalOffset.center,
                                    margin: new EdgeInsets.fromLTRB(
                                        0.0, 0.0, 8.0, 0.0),
                                    height: 52.0,
                                    width: 24.0,
                                    decoration: new BoxDecoration(
                                        color: wunderlistRed))),
                            new Container(
                                alignment: FractionalOffset.topLeft,
                                margin: new EdgeInsets.fromLTRB(
                                    3.0, 19.0, 0.0, 0.0),
                                child: new AnimatedCrossFade(
                                    crossFadeState: todo.isFavorite
                                        ? CrossFadeState.showFirst
                                        : CrossFadeState.showSecond,
                                    duration: new Duration(milliseconds: 150),
                                    firstChild: new Icon(Icons.star,
                                        size: 18.0, color: white),
                                    secondChild: new Icon(Icons.star_border,
                                        size: 18.0, color: lightGray))),
                            new Positioned(
                                bottom: -36.0,
                                left: -28.0,
                                child: new Icon(Icons.arrow_drop_up,
                                    size: 80.0, color: white))
                          ]))))
            ])));
  }
}
