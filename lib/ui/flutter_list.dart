import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_list/actions/add_todo.dart';
import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/actions/remove_completed.dart';
import 'package:flutter_list/injection/app_module.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/selectors/todos_selector.dart';
import 'package:flutter_list/ui/colors.dart';
import 'package:flutter_list/ui/flutter_list_model.dart';
import 'package:flutter_list/ui/todo_form.dart';
import 'package:flutter_list/ui/todo_widget.dart';
import 'package:flutter_stream_friends/flutter_stream_friends.dart';
import 'package:redux/redux.dart';
import 'package:rxdart/rxdart.dart';

class FlutterList extends StreamWidget<FlutterListModel> {
  final Store<AppState, AppAction> store;
  final TodosSelector todoSelector;

  FlutterList(this.todoSelector, {Key key, Store<AppState, AppAction> store})
      : this.store = store ?? new AppModule().store,
        super(key: key);

  @override
  Stream<FlutterListModel> createStream() {
    final TextEditingController textEditingController =
        new TextEditingController();
    final ValueChangedStreamCallback<String> onChange =
        new ValueChangedStreamCallback<String>();
    final ValueChangedStreamCallback<String> onSubmit =
        new ValueChangedStreamCallback<String>();
    final TapStreamCallback onAddTapped = new TapStreamCallback();

    var distinctOnChange = new Observable<String>(onChange).distinct();

    final Stream<bool> isEditing = distinctOnChange
        .map((task) => task.isNotEmpty)
        .startWith(false)
        .distinct();

    final Observable<String> onAdd = new Observable(onAddTapped)
        .withLatestFrom(distinctOnChange, (_, task) => task)
        .distinct();

    final Stream<String> onSubmitValue = new Observable.merge([onSubmit, onAdd])
        .where((task) => task.isNotEmpty)
        .call(
            onData: (task) => store.dispatch(new AddTodo((b) => b
              ..task = task
              ..isComplete = false
              ..isFavorite = false)))
        .map((_) => "");

    final Stream<String> task =
        new Observable.merge([distinctOnChange, onSubmitValue]).startWith("");

    return Observable.combineLatest4(
        isEditing, todoSelector.onTodosChange, task, todoSelector.hasCompleted,
        (isEditing, todos, task, hasCompleted) {
      return new FlutterListModel((b) => b
        ..onClearCompletedTapped =
            (() => store.dispatch(new RemoveCompletedTodos()))
        ..todos = todos
        ..textEditingController = textEditingController
        ..hasCompleted = hasCompleted
        ..todoFormModel = new TodoFormModel(onChange, onSubmit, onAddTapped,
            task, isEditing, textEditingController));
    });
  }

  @override
  Widget build(BuildContext context, FlutterListModel vm) {
    var screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        drawer: new Drawer(
            child: new Stack(children: [
          new Positioned(
              right: 16.0,
              bottom: 16.0,
              child: new FloatingActionButton(
                  backgroundColor: wunderlistBlue,
                  child: new Icon(Icons.add),
                  onPressed: () {}))
        ])),
        appBar: new AppBar(
          title: new Text('Current Todo List',
              style: new TextStyle(
                  fontWeight: FontWeight.bold, fontFamily: 'Lato')),
        ),
        body: new Stack(children: <Widget>[
          new Container(
            width: screenSize.width,
            height: screenSize.height,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("assets/images/berlin.jpg"),
                  fit: BoxFit.cover),
            ),
            child: new Container(
              padding: new EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
              child: new ListView(
                itemExtent: 60.0,
                padding: new EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 16.0),
                children: _buildListItems(vm),
              ),
            ),
          ),
        ]));
  }

  List<Widget> _buildListItems(FlutterListModel vm) =>
      <Widget>[new TodoForm(vm.todoFormModel)]
        ..addAll(vm.todos.map((todo) {
          var widget = new TodoWidget(todo);
          return widget;
        }))
        ..add(new AnimatedOpacity(
            duration: new Duration(milliseconds: 100),
            opacity: vm.hasCompleted ? 1.0 : 0.0,
            child: new Material(
                color: Colors.transparent,
                child: new Container(
                    margin: new EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 24.0),
                    decoration: new BoxDecoration(
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(4.0)),
                        color: const Color(0x55000000)),
                    child: new InkWell(
                        onTap: vm.onClearCompletedTapped,
                        child: new Container(
                            alignment: FractionalOffset.center,
                            child: new Text('Clear Completed',
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Lato'))))))));

  @override
  Widget buildLoading(BuildContext context) {
    return build(context, new FlutterListModel((b) {
      final textEditingController = new TextEditingController();
      return b
        ..onClearCompletedTapped = () {}
        ..todos = []
        ..textEditingController = textEditingController
        ..hasCompleted = false
        ..todoFormModel = new TodoFormModel(
            (_) {}, (_) {}, () {}, "", false, textEditingController);
    }));
  }
}
