import 'dart:ui';

Color wunderlistRed = new Color(0xFFDB4C3F);
Color wunderlistBlue = new Color(0xFF2B88D9);
Color wunderlistGreen = new Color(0xFF65B01B);
Color wunderlistYellow = new Color(0xFFE6B035);
Color darkGray = new Color(0xFF444444);
Color wunderlistGray = new Color(0xFF5B5B5B);
Color gray = new Color(0xFF737273);
Color lightGray = new Color(0xFFA3A3A3);
Color white = new Color(0xFFFAFAFA);
