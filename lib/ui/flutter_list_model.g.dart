// GENERATED CODE - DO NOT MODIFY BY HAND

part of flutter_list_model;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class FlutterListModel
// **************************************************************************

class _$FlutterListModel extends FlutterListModel {
  @override
  final Iterable<Todo> todos;
  @override
  final TodoFormModel todoFormModel;
  @override
  final bool hasCompleted;
  @override
  final GestureTapCallback onClearCompletedTapped;
  @override
  final TextEditingController textEditingController;

  factory _$FlutterListModel([updates(FlutterListModelBuilder b)]) =>
      (new FlutterListModelBuilder()..update(updates)).build();

  _$FlutterListModel._(
      {this.todos,
      this.todoFormModel,
      this.hasCompleted,
      this.onClearCompletedTapped,
      this.textEditingController})
      : super._() {
    if (todos == null) throw new ArgumentError.notNull('todos');
    if (todoFormModel == null) throw new ArgumentError.notNull('todoFormModel');
    if (hasCompleted == null) throw new ArgumentError.notNull('hasCompleted');
    if (onClearCompletedTapped == null)
      throw new ArgumentError.notNull('onClearCompletedTapped');
    if (textEditingController == null)
      throw new ArgumentError.notNull('textEditingController');
  }

  @override
  FlutterListModel rebuild(updates(FlutterListModelBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  FlutterListModelBuilder toBuilder() =>
      new FlutterListModelBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! FlutterListModel) return false;
    return todos == other.todos &&
        todoFormModel == other.todoFormModel &&
        hasCompleted == other.hasCompleted &&
        onClearCompletedTapped == other.onClearCompletedTapped &&
        textEditingController == other.textEditingController;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, todos.hashCode), todoFormModel.hashCode),
                hasCompleted.hashCode),
            onClearCompletedTapped.hashCode),
        textEditingController.hashCode));
  }

  @override
  String toString() {
    return 'FlutterListModel {'
        'todos=${todos.toString()},\n'
        'todoFormModel=${todoFormModel.toString()},\n'
        'hasCompleted=${hasCompleted.toString()},\n'
        'onClearCompletedTapped=${onClearCompletedTapped.toString()},\n'
        'textEditingController=${textEditingController.toString()},\n'
        '}';
  }
}

class FlutterListModelBuilder
    implements Builder<FlutterListModel, FlutterListModelBuilder> {
  FlutterListModel _$v;

  Iterable<Todo> _todos;
  Iterable<Todo> get todos => _$this._todos;
  set todos(Iterable<Todo> todos) => _$this._todos = todos;

  TodoFormModel _todoFormModel;
  TodoFormModel get todoFormModel => _$this._todoFormModel;
  set todoFormModel(TodoFormModel todoFormModel) =>
      _$this._todoFormModel = todoFormModel;

  bool _hasCompleted;
  bool get hasCompleted => _$this._hasCompleted;
  set hasCompleted(bool hasCompleted) => _$this._hasCompleted = hasCompleted;

  GestureTapCallback _onClearCompletedTapped;
  GestureTapCallback get onClearCompletedTapped =>
      _$this._onClearCompletedTapped;
  set onClearCompletedTapped(GestureTapCallback onClearCompletedTapped) =>
      _$this._onClearCompletedTapped = onClearCompletedTapped;

  TextEditingController _textEditingController;
  TextEditingController get textEditingController =>
      _$this._textEditingController;
  set textEditingController(TextEditingController textEditingController) =>
      _$this._textEditingController = textEditingController;

  FlutterListModelBuilder();

  FlutterListModelBuilder get _$this {
    if (_$v != null) {
      _todos = _$v.todos;
      _todoFormModel = _$v.todoFormModel;
      _hasCompleted = _$v.hasCompleted;
      _onClearCompletedTapped = _$v.onClearCompletedTapped;
      _textEditingController = _$v.textEditingController;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FlutterListModel other) {
    _$v = other;
  }

  @override
  void update(updates(FlutterListModelBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  FlutterListModel build() {
    final result = _$v ??
        new _$FlutterListModel._(
            todos: todos,
            todoFormModel: todoFormModel,
            hasCompleted: hasCompleted,
            onClearCompletedTapped: onClearCompletedTapped,
            textEditingController: textEditingController);
    replace(result);
    return result;
  }
}
