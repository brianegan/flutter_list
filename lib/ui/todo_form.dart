import 'package:flutter/material.dart';
import 'package:flutter/src/services/text_editing.dart';
import 'package:flutter/widgets.dart';

class TodoForm extends StatelessWidget {
  final TodoFormModel vm;
  TextEditingController textEditingController = new TextEditingController();

  TodoForm(this.vm, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (vm.task.isEmpty) {
      vm.textEditingController.text = vm.task;
    }

    return new Container(
        height: 60.0,
        margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
        padding: new EdgeInsets.fromLTRB(16.0, 0.0, 0.0, 0.0),
        decoration: new BoxDecoration(
            borderRadius: new BorderRadius.circular(3.0),
            color: new Color.fromARGB(120, 88, 141, 100)),
        child: new Row(children: [
          new Flexible(
              fit: FlexFit.tight,
              child: new TextField(
                  decoration: new InputDecoration(
                      hintText: "Add a todo...", hideDivider: true),
                  style: new TextStyle(fontFamily: 'Lato', fontSize: 16.0),
                  controller: vm.textEditingController,
                  onChanged: vm.onChanged,
                  onSubmitted: vm.onSubmitted)),
          new Container(
              height: 40.0,
              width: 32.0,
              padding: new EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 0.0),
              child: new GestureDetector(
                  onTap: vm.onAddTapped,
                  child: new AnimatedOpacity(
                      duration: new Duration(milliseconds: 150),
                      opacity: vm.isEditing ? 1.0 : 0.5,
                      child: new Text('+',
                          style: new TextStyle(
                              color: Colors.white,
                              fontFamily: 'Lato',
                              fontSize: 32.0))))),
        ]));
  }
}

class TodoFormModel {
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final GestureTapCallback onAddTapped;
  final String task;
  final bool isEditing;
  final TextEditingController textEditingController;

  TodoFormModel(this.onChanged, this.onSubmitted, this.onAddTapped, this.task,
      this.isEditing, this.textEditingController);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoFormModel &&
          runtimeType == other.runtimeType &&
          onChanged == other.onChanged &&
          onSubmitted == other.onSubmitted &&
          onAddTapped == other.onAddTapped &&
          task == other.task &&
          isEditing == other.isEditing &&
          textEditingController == other.textEditingController;

  @override
  int get hashCode =>
      onChanged.hashCode ^
      onSubmitted.hashCode ^
      onAddTapped.hashCode ^
      task.hashCode ^
      isEditing.hashCode ^
      textEditingController.hashCode;

  @override
  String toString() {
    return 'TodoFormModel{onChanged: $onChanged, onSubmitted: $onSubmitted, onAddTapped: $onAddTapped, task: $task, isEditing: $isEditing, textEditingController: $textEditingController}';
  }
}
