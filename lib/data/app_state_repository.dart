import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/models/serializers.dart';

// The class responsible for saving and loading Application State to disk. Could
// eventually encapsulate persisting to the cloud or Firebase if necessary.
class AppStateRepository {
  final FileProvider fileProvider;

  AppStateRepository({FileProvider fileProvider})
      : this.fileProvider = fileProvider ?? _defaultAppStateFile;

  Future<AppState> loadState() async {
    var file = await fileProvider();

    if (await file.exists()) {
      try {
        return serializers.deserialize(JSON.decode(await file.readAsString()));
      } catch (e) {
        return new AppState.empty();
      }
    } else {
      return new AppState.empty();
    }
  }

  Future<File> saveState(AppState state) async {
    var file = await fileProvider();

    return file.writeAsString(JSON.encode(serializers.serialize(state)));
  }

  static Future<File> _defaultAppStateFile() async {
    var directory = await getApplicationDocumentsDirectory();

    return new File('${directory.path}/app_state.json');
  }
}

typedef Future<File> FileProvider();
