import 'dart:async';

import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/injection/app_module.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/models/todo.dart';
import 'package:redux/redux.dart';
import 'package:rxdart/rxdart.dart';

// A helper class to Stream only a part of the application state you might
// care about. Helpful to reduce repeating these types of queries.
class TodosSelector {
  final Store<AppState, AppAction> store;

  TodosSelector({Store<AppState, AppAction> store})
      : this.store = store ?? new AppModule().store; // Poor Man's DI

  Stream<Iterable<Todo>> get onTodosChange => new Observable(store.onChange)
      .map((state) => state.todos)
      .startWith(store.state.todos)
      .distinct();

  Stream<bool> get hasCompleted {
    return onTodosChange.map((todos) => todos.any((todo) => todo.isComplete));
  }
}
