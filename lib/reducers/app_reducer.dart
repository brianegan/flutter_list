import 'package:flutter_list/actions/add_todo.dart';
import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/actions/init.dart';
import 'package:flutter_list/actions/remove_completed.dart';
import 'package:flutter_list/actions/remove_todo.dart';
import 'package:flutter_list/actions/state_loaded.dart';
import 'package:flutter_list/actions/toggle_todo_complete.dart';
import 'package:flutter_list/actions/toggle_todo_favorite.dart';
import 'package:flutter_list/actions/undo_remove.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/models/todo.dart';
import 'package:redux/redux.dart';

class AppReducer extends Reducer<AppState, AppAction> {
  @override
  AppState reduce(AppState state, AppAction action) {
    if (action is Init) {
      return new AppState.of([]);
    } else if (action is StateLoaded) {
      return action.state;
    } else if (action is AddTodo) {
      var todo = new Todo.from(action.task);

      return new AppState.of((state.todos.toBuilder()..add(todo)).build());
    } else if (action is ToggleTodoComplete) {
      return new AppState.of(state.todos.map((Todo todo) => todo.id == action.id
          ? (todo.toBuilder()..isComplete = action.isComplete).build()
          : todo));
    } else if (action is ToggleTodoFavorite) {
      return new AppState.of(state.todos.map((todo) => todo.id == action.id
          ? (todo.toBuilder()..isFavorite = action.isFavorite).build()
          : todo));
    } else if (action is RemoveTodo) {
      return new AppState.of(state.todos.where((todo) => todo.id != action.id));
    } else if (action is RemoveCompletedTodos) {
      return new AppState.of(state.todos.where((todo) => !todo.isComplete));
    } else if (action is UndoRemoveTodo) {
      return new AppState.of(
          (state.todos.toBuilder()..add(action.todo)).build());
    }

    return state;
  }
}
