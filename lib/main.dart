import 'package:flutter/material.dart';
import 'package:flutter_list/ui/colors.dart';
import 'package:flutter_list/selectors/todos_selector.dart';
import 'package:flutter_list/ui/flutter_list.dart';

void main() {
  runApp(
    new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
          primaryColor: new Color(0xFF588d64),
          accentColor: wunderlistBlue,
          hintColor: Colors.white),
      home: new FlutterList(new TodosSelector()),
    ),
  );
}
