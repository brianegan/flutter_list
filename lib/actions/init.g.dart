// GENERATED CODE - DO NOT MODIFY BY HAND

part of init;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library init
// **************************************************************************

Serializer<Init> _$initSerializer = new _$InitSerializer();

class _$InitSerializer implements StructuredSerializer<Init> {
  @override
  final Iterable<Type> types = const [Init, _$Init];
  @override
  final String wireName = 'Init';

  @override
  Iterable serialize(Serializers serializers, Init object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [];

    return result;
  }

  @override
  Init deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new InitBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class Init
// **************************************************************************

class _$Init extends Init {
  factory _$Init([updates(InitBuilder b)]) =>
      (new InitBuilder()..update(updates)).build();

  _$Init._() : super._();

  @override
  Init rebuild(updates(InitBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  InitBuilder toBuilder() => new InitBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! Init) return false;
    return true;
  }

  @override
  int get hashCode {
    return 434862477;
  }

  @override
  String toString() {
    return 'Init {}';
  }
}

class InitBuilder implements Builder<Init, InitBuilder> {
  Init _$v;

  InitBuilder();

  InitBuilder get _$this {
    if (_$v != null) {
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Init other) {
    _$v = other;
  }

  @override
  void update(updates(InitBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  Init build() {
    final result = _$v ?? new _$Init._();
    replace(result);
    return result;
  }
}
