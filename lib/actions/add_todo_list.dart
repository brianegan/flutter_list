library add_todo_list;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'add_todo_list.g.dart';

abstract class AddTodoList
    implements Built<AddTodoList, AddTodoListBuilder>, AppAction {
  static Serializer<AddTodoList> get serializer => _$addTodoListSerializer;

  String get name;

  AddTodoList._();

  factory AddTodoList([updates(AddTodoListBuilder b)]) = _$AddTodoList;
}
