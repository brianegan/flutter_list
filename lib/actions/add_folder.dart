library add_folder;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'add_folder.g.dart';

abstract class AddFolder
    implements Built<AddFolder, AddFolderBuilder>, AppAction {
  static Serializer<AddFolder> get serializer => _$addFolderSerializer;

  String get name;

  AddFolder._();

  factory AddFolder([updates(AddFolderBuilder b)]) = _$AddFolder;
}
