// GENERATED CODE - DO NOT MODIFY BY HAND

part of state_load_error;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library state_load_error
// **************************************************************************

Serializer<StateLoadError> _$stateLoadErrorSerializer =
    new _$StateLoadErrorSerializer();

class _$StateLoadErrorSerializer
    implements StructuredSerializer<StateLoadError> {
  @override
  final Iterable<Type> types = const [StateLoadError, _$StateLoadError];
  @override
  final String wireName = 'StateLoadError';

  @override
  Iterable serialize(Serializers serializers, StateLoadError object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [];

    return result;
  }

  @override
  StateLoadError deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new StateLoadErrorBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class StateLoadError
// **************************************************************************

class _$StateLoadError extends StateLoadError {
  factory _$StateLoadError([updates(StateLoadErrorBuilder b)]) =>
      (new StateLoadErrorBuilder()..update(updates)).build();

  _$StateLoadError._() : super._();

  @override
  StateLoadError rebuild(updates(StateLoadErrorBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  StateLoadErrorBuilder toBuilder() =>
      new StateLoadErrorBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! StateLoadError) return false;
    return true;
  }

  @override
  int get hashCode {
    return 583604668;
  }

  @override
  String toString() {
    return 'StateLoadError {}';
  }
}

class StateLoadErrorBuilder
    implements Builder<StateLoadError, StateLoadErrorBuilder> {
  StateLoadError _$v;

  StateLoadErrorBuilder();

  StateLoadErrorBuilder get _$this {
    if (_$v != null) {
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StateLoadError other) {
    _$v = other;
  }

  @override
  void update(updates(StateLoadErrorBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  StateLoadError build() {
    final result = _$v ?? new _$StateLoadError._();
    replace(result);
    return result;
  }
}
