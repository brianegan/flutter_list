// GENERATED CODE - DO NOT MODIFY BY HAND

part of toggle_todo_complete;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library toggle_todo_complete
// **************************************************************************

Serializer<ToggleTodoComplete> _$toggleTodoCompleteSerializer =
    new _$ToggleTodoCompleteSerializer();

class _$ToggleTodoCompleteSerializer
    implements StructuredSerializer<ToggleTodoComplete> {
  @override
  final Iterable<Type> types = const [ToggleTodoComplete, _$ToggleTodoComplete];
  @override
  final String wireName = 'ToggleTodoComplete';

  @override
  Iterable serialize(Serializers serializers, ToggleTodoComplete object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'isComplete',
      serializers.serialize(object.isComplete,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  ToggleTodoComplete deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new ToggleTodoCompleteBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'id':
            result.id = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
          case 'isComplete':
            result.isComplete = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class ToggleTodoComplete
// **************************************************************************

class _$ToggleTodoComplete extends ToggleTodoComplete {
  @override
  final String id;
  @override
  final bool isComplete;

  factory _$ToggleTodoComplete([updates(ToggleTodoCompleteBuilder b)]) =>
      (new ToggleTodoCompleteBuilder()..update(updates)).build();

  _$ToggleTodoComplete._({this.id, this.isComplete}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (isComplete == null) throw new ArgumentError.notNull('isComplete');
  }

  @override
  ToggleTodoComplete rebuild(updates(ToggleTodoCompleteBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ToggleTodoCompleteBuilder toBuilder() =>
      new ToggleTodoCompleteBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! ToggleTodoComplete) return false;
    return id == other.id && isComplete == other.isComplete;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), isComplete.hashCode));
  }

  @override
  String toString() {
    return 'ToggleTodoComplete {'
        'id=${id.toString()},\n'
        'isComplete=${isComplete.toString()},\n'
        '}';
  }
}

class ToggleTodoCompleteBuilder
    implements Builder<ToggleTodoComplete, ToggleTodoCompleteBuilder> {
  ToggleTodoComplete _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _isComplete;
  bool get isComplete => _$this._isComplete;
  set isComplete(bool isComplete) => _$this._isComplete = isComplete;

  ToggleTodoCompleteBuilder();

  ToggleTodoCompleteBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _isComplete = _$v.isComplete;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ToggleTodoComplete other) {
    _$v = other;
  }

  @override
  void update(updates(ToggleTodoCompleteBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  ToggleTodoComplete build() {
    final result =
        _$v ?? new _$ToggleTodoComplete._(id: id, isComplete: isComplete);
    replace(result);
    return result;
  }
}
