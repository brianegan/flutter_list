library remove_todo;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'remove_todo.g.dart';

abstract class RemoveTodo
    implements Built<RemoveTodo, RemoveTodoBuilder>, AppAction {
  static Serializer<RemoveTodo> get serializer => _$removeTodoSerializer;

  String get id;

  RemoveTodo._();

  factory RemoveTodo([updates(RemoveTodoBuilder b)]) = _$RemoveTodo;
}
