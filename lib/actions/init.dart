library init;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'init.g.dart';

abstract class Init implements Built<Init, InitBuilder>, AppAction {
  static Serializer<Init> get serializer => _$initSerializer;

  Init._();

  factory Init([updates(InitBuilder b)]) = _$Init;
}
