library state_loaded;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/models/app_state.dart';

part 'state_loaded.g.dart';

abstract class StateLoaded
    implements Built<StateLoaded, StateLoadedBuilder>, AppAction {
  static Serializer<StateLoaded> get serializer => _$stateLoadedSerializer;

  AppState get state;

  StateLoaded._();

  factory StateLoaded([updates(StateLoadedBuilder b)]) = _$StateLoaded;
}
