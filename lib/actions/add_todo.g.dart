// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_todo;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library add_todo
// **************************************************************************

Serializer<AddTodo> _$addTodoSerializer = new _$AddTodoSerializer();

class _$AddTodoSerializer implements StructuredSerializer<AddTodo> {
  @override
  final Iterable<Type> types = const [AddTodo, _$AddTodo];
  @override
  final String wireName = 'AddTodo';

  @override
  Iterable serialize(Serializers serializers, AddTodo object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'task',
      serializers.serialize(object.task, specifiedType: const FullType(String)),
      'isComplete',
      serializers.serialize(object.isComplete,
          specifiedType: const FullType(bool)),
      'isFavorite',
      serializers.serialize(object.isFavorite,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  AddTodo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new AddTodoBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'task':
            result.task = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
          case 'isComplete':
            result.isComplete = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
          case 'isFavorite':
            result.isFavorite = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class AddTodo
// **************************************************************************

class _$AddTodo extends AddTodo {
  @override
  final String task;
  @override
  final bool isComplete;
  @override
  final bool isFavorite;

  factory _$AddTodo([updates(AddTodoBuilder b)]) =>
      (new AddTodoBuilder()..update(updates)).build();

  _$AddTodo._({this.task, this.isComplete, this.isFavorite}) : super._() {
    if (task == null) throw new ArgumentError.notNull('task');
    if (isComplete == null) throw new ArgumentError.notNull('isComplete');
    if (isFavorite == null) throw new ArgumentError.notNull('isFavorite');
  }

  @override
  AddTodo rebuild(updates(AddTodoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AddTodoBuilder toBuilder() => new AddTodoBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! AddTodo) return false;
    return task == other.task &&
        isComplete == other.isComplete &&
        isFavorite == other.isFavorite;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, task.hashCode), isComplete.hashCode), isFavorite.hashCode));
  }

  @override
  String toString() {
    return 'AddTodo {'
        'task=${task.toString()},\n'
        'isComplete=${isComplete.toString()},\n'
        'isFavorite=${isFavorite.toString()},\n'
        '}';
  }
}

class AddTodoBuilder implements Builder<AddTodo, AddTodoBuilder> {
  AddTodo _$v;

  String _task;
  String get task => _$this._task;
  set task(String task) => _$this._task = task;

  bool _isComplete;
  bool get isComplete => _$this._isComplete;
  set isComplete(bool isComplete) => _$this._isComplete = isComplete;

  bool _isFavorite;
  bool get isFavorite => _$this._isFavorite;
  set isFavorite(bool isFavorite) => _$this._isFavorite = isFavorite;

  AddTodoBuilder();

  AddTodoBuilder get _$this {
    if (_$v != null) {
      _task = _$v.task;
      _isComplete = _$v.isComplete;
      _isFavorite = _$v.isFavorite;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddTodo other) {
    _$v = other;
  }

  @override
  void update(updates(AddTodoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  AddTodo build() {
    final result = _$v ??
        new _$AddTodo._(
            task: task, isComplete: isComplete, isFavorite: isFavorite);
    replace(result);
    return result;
  }
}
