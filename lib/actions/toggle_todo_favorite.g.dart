// GENERATED CODE - DO NOT MODIFY BY HAND

part of toggle_todo_favorite;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library toggle_todo_favorite
// **************************************************************************

Serializer<ToggleTodoFavorite> _$toggleTodoFavoriteSerializer =
    new _$ToggleTodoFavoriteSerializer();

class _$ToggleTodoFavoriteSerializer
    implements StructuredSerializer<ToggleTodoFavorite> {
  @override
  final Iterable<Type> types = const [ToggleTodoFavorite, _$ToggleTodoFavorite];
  @override
  final String wireName = 'ToggleTodoFavorite';

  @override
  Iterable serialize(Serializers serializers, ToggleTodoFavorite object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'isFavorite',
      serializers.serialize(object.isFavorite,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  ToggleTodoFavorite deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new ToggleTodoFavoriteBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'id':
            result.id = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
          case 'isFavorite':
            result.isFavorite = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class ToggleTodoFavorite
// **************************************************************************

class _$ToggleTodoFavorite extends ToggleTodoFavorite {
  @override
  final String id;
  @override
  final bool isFavorite;

  factory _$ToggleTodoFavorite([updates(ToggleTodoFavoriteBuilder b)]) =>
      (new ToggleTodoFavoriteBuilder()..update(updates)).build();

  _$ToggleTodoFavorite._({this.id, this.isFavorite}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (isFavorite == null) throw new ArgumentError.notNull('isFavorite');
  }

  @override
  ToggleTodoFavorite rebuild(updates(ToggleTodoFavoriteBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ToggleTodoFavoriteBuilder toBuilder() =>
      new ToggleTodoFavoriteBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! ToggleTodoFavorite) return false;
    return id == other.id && isFavorite == other.isFavorite;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), isFavorite.hashCode));
  }

  @override
  String toString() {
    return 'ToggleTodoFavorite {'
        'id=${id.toString()},\n'
        'isFavorite=${isFavorite.toString()},\n'
        '}';
  }
}

class ToggleTodoFavoriteBuilder
    implements Builder<ToggleTodoFavorite, ToggleTodoFavoriteBuilder> {
  ToggleTodoFavorite _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _isFavorite;
  bool get isFavorite => _$this._isFavorite;
  set isFavorite(bool isFavorite) => _$this._isFavorite = isFavorite;

  ToggleTodoFavoriteBuilder();

  ToggleTodoFavoriteBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _isFavorite = _$v.isFavorite;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ToggleTodoFavorite other) {
    _$v = other;
  }

  @override
  void update(updates(ToggleTodoFavoriteBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  ToggleTodoFavorite build() {
    final result =
        _$v ?? new _$ToggleTodoFavorite._(id: id, isFavorite: isFavorite);
    replace(result);
    return result;
  }
}
