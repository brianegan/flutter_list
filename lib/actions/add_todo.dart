library add_todo;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'add_todo.g.dart';

abstract class AddTodo implements Built<AddTodo, AddTodoBuilder>, AppAction {
  static Serializer<AddTodo> get serializer => _$addTodoSerializer;

  String get task;

  bool get isComplete;

  bool get isFavorite;

  AddTodo._();

  factory AddTodo([updates(AddTodoBuilder b)]) = _$AddTodo;
}
