// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_todo_list;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library add_todo_list
// **************************************************************************

Serializer<AddTodoList> _$addTodoListSerializer = new _$AddTodoListSerializer();

class _$AddTodoListSerializer implements StructuredSerializer<AddTodoList> {
  @override
  final Iterable<Type> types = const [AddTodoList, _$AddTodoList];
  @override
  final String wireName = 'AddTodoList';

  @override
  Iterable serialize(Serializers serializers, AddTodoList object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  AddTodoList deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new AddTodoListBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'name':
            result.name = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class AddTodoList
// **************************************************************************

class _$AddTodoList extends AddTodoList {
  @override
  final String name;

  factory _$AddTodoList([updates(AddTodoListBuilder b)]) =>
      (new AddTodoListBuilder()..update(updates)).build();

  _$AddTodoList._({this.name}) : super._() {
    if (name == null) throw new ArgumentError.notNull('name');
  }

  @override
  AddTodoList rebuild(updates(AddTodoListBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AddTodoListBuilder toBuilder() => new AddTodoListBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! AddTodoList) return false;
    return name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc(0, name.hashCode));
  }

  @override
  String toString() {
    return 'AddTodoList {'
        'name=${name.toString()},\n'
        '}';
  }
}

class AddTodoListBuilder implements Builder<AddTodoList, AddTodoListBuilder> {
  AddTodoList _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  AddTodoListBuilder();

  AddTodoListBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddTodoList other) {
    _$v = other;
  }

  @override
  void update(updates(AddTodoListBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  AddTodoList build() {
    final result = _$v ?? new _$AddTodoList._(name: name);
    replace(result);
    return result;
  }
}
