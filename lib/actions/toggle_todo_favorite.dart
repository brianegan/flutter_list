library toggle_todo_favorite;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'toggle_todo_favorite.g.dart';

abstract class ToggleTodoFavorite
    implements Built<ToggleTodoFavorite, ToggleTodoFavoriteBuilder>, AppAction {
  static Serializer<ToggleTodoFavorite> get serializer =>
      _$toggleTodoFavoriteSerializer;

  String get id;

  bool get isFavorite;

  ToggleTodoFavorite._();

  factory ToggleTodoFavorite([updates(ToggleTodoFavoriteBuilder b)]) =
      _$ToggleTodoFavorite;
}
