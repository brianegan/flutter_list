// GENERATED CODE - DO NOT MODIFY BY HAND

part of undo_remove;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library undo_remove
// **************************************************************************

Serializer<UndoRemoveTodo> _$undoRemoveTodoSerializer =
    new _$UndoRemoveTodoSerializer();

class _$UndoRemoveTodoSerializer
    implements StructuredSerializer<UndoRemoveTodo> {
  @override
  final Iterable<Type> types = const [UndoRemoveTodo, _$UndoRemoveTodo];
  @override
  final String wireName = 'UndoRemoveTodo';

  @override
  Iterable serialize(Serializers serializers, UndoRemoveTodo object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'todo',
      serializers.serialize(object.todo, specifiedType: const FullType(Todo)),
    ];

    return result;
  }

  @override
  UndoRemoveTodo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new UndoRemoveTodoBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'todo':
            result.todo.replace(serializers.deserialize(value,
                specifiedType: const FullType(Todo)));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class UndoRemoveTodo
// **************************************************************************

class _$UndoRemoveTodo extends UndoRemoveTodo {
  @override
  final Todo todo;

  factory _$UndoRemoveTodo([updates(UndoRemoveTodoBuilder b)]) =>
      (new UndoRemoveTodoBuilder()..update(updates)).build();

  _$UndoRemoveTodo._({this.todo}) : super._() {
    if (todo == null) throw new ArgumentError.notNull('todo');
  }

  @override
  UndoRemoveTodo rebuild(updates(UndoRemoveTodoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UndoRemoveTodoBuilder toBuilder() =>
      new UndoRemoveTodoBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! UndoRemoveTodo) return false;
    return todo == other.todo;
  }

  @override
  int get hashCode {
    return $jf($jc(0, todo.hashCode));
  }

  @override
  String toString() {
    return 'UndoRemoveTodo {'
        'todo=${todo.toString()},\n'
        '}';
  }
}

class UndoRemoveTodoBuilder
    implements Builder<UndoRemoveTodo, UndoRemoveTodoBuilder> {
  UndoRemoveTodo _$v;

  TodoBuilder _todo;
  TodoBuilder get todo => _$this._todo ??= new TodoBuilder();
  set todo(TodoBuilder todo) => _$this._todo = todo;

  UndoRemoveTodoBuilder();

  UndoRemoveTodoBuilder get _$this {
    if (_$v != null) {
      _todo = _$v.todo?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UndoRemoveTodo other) {
    _$v = other;
  }

  @override
  void update(updates(UndoRemoveTodoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  UndoRemoveTodo build() {
    final result = _$v ?? new _$UndoRemoveTodo._(todo: todo?.build());
    replace(result);
    return result;
  }
}
