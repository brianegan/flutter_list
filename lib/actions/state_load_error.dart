library state_load_error;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'state_load_error.g.dart';

abstract class StateLoadError
    implements Built<StateLoadError, StateLoadErrorBuilder>, AppAction {
  static Serializer<StateLoadError> get serializer =>
      _$stateLoadErrorSerializer;

  StateLoadError._();

  factory StateLoadError([updates(StateLoadErrorBuilder b)]) = _$StateLoadError;
}
