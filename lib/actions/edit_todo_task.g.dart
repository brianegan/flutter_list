// GENERATED CODE - DO NOT MODIFY BY HAND

part of edit_todo_task;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library edit_todo_task
// **************************************************************************

Serializer<EditTodoTask> _$editTodoTaskSerializer =
    new _$EditTodoTaskSerializer();

class _$EditTodoTaskSerializer implements StructuredSerializer<EditTodoTask> {
  @override
  final Iterable<Type> types = const [EditTodoTask, _$EditTodoTask];
  @override
  final String wireName = 'EditTodoTask';

  @override
  Iterable serialize(Serializers serializers, EditTodoTask object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'task',
      serializers.serialize(object.task, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EditTodoTask deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new EditTodoTaskBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'id':
            result.id = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
          case 'task':
            result.task = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class EditTodoTask
// **************************************************************************

class _$EditTodoTask extends EditTodoTask {
  @override
  final String id;
  @override
  final String task;

  factory _$EditTodoTask([updates(EditTodoTaskBuilder b)]) =>
      (new EditTodoTaskBuilder()..update(updates)).build();

  _$EditTodoTask._({this.id, this.task}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (task == null) throw new ArgumentError.notNull('task');
  }

  @override
  EditTodoTask rebuild(updates(EditTodoTaskBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EditTodoTaskBuilder toBuilder() => new EditTodoTaskBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! EditTodoTask) return false;
    return id == other.id && task == other.task;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), task.hashCode));
  }

  @override
  String toString() {
    return 'EditTodoTask {'
        'id=${id.toString()},\n'
        'task=${task.toString()},\n'
        '}';
  }
}

class EditTodoTaskBuilder
    implements Builder<EditTodoTask, EditTodoTaskBuilder> {
  EditTodoTask _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _task;
  String get task => _$this._task;
  set task(String task) => _$this._task = task;

  EditTodoTaskBuilder();

  EditTodoTaskBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _task = _$v.task;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EditTodoTask other) {
    _$v = other;
  }

  @override
  void update(updates(EditTodoTaskBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  EditTodoTask build() {
    final result = _$v ?? new _$EditTodoTask._(id: id, task: task);
    replace(result);
    return result;
  }
}
