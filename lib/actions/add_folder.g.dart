// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_folder;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library add_folder
// **************************************************************************

Serializer<AddFolder> _$addFolderSerializer = new _$AddFolderSerializer();

class _$AddFolderSerializer implements StructuredSerializer<AddFolder> {
  @override
  final Iterable<Type> types = const [AddFolder, _$AddFolder];
  @override
  final String wireName = 'AddFolder';

  @override
  Iterable serialize(Serializers serializers, AddFolder object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  AddFolder deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new AddFolderBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'name':
            result.name = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class AddFolder
// **************************************************************************

class _$AddFolder extends AddFolder {
  @override
  final String name;

  factory _$AddFolder([updates(AddFolderBuilder b)]) =>
      (new AddFolderBuilder()..update(updates)).build();

  _$AddFolder._({this.name}) : super._() {
    if (name == null) throw new ArgumentError.notNull('name');
  }

  @override
  AddFolder rebuild(updates(AddFolderBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AddFolderBuilder toBuilder() => new AddFolderBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! AddFolder) return false;
    return name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc(0, name.hashCode));
  }

  @override
  String toString() {
    return 'AddFolder {'
        'name=${name.toString()},\n'
        '}';
  }
}

class AddFolderBuilder implements Builder<AddFolder, AddFolderBuilder> {
  AddFolder _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  AddFolderBuilder();

  AddFolderBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddFolder other) {
    _$v = other;
  }

  @override
  void update(updates(AddFolderBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  AddFolder build() {
    final result = _$v ?? new _$AddFolder._(name: name);
    replace(result);
    return result;
  }
}
