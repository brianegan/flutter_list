// GENERATED CODE - DO NOT MODIFY BY HAND

part of remove_completed;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library remove_completed
// **************************************************************************

Serializer<RemoveCompletedTodos> _$removeCompletedTodosSerializer =
    new _$RemoveCompletedTodosSerializer();

class _$RemoveCompletedTodosSerializer
    implements StructuredSerializer<RemoveCompletedTodos> {
  @override
  final Iterable<Type> types = const [
    RemoveCompletedTodos,
    _$RemoveCompletedTodos
  ];
  @override
  final String wireName = 'RemoveCompletedTodos';

  @override
  Iterable serialize(Serializers serializers, RemoveCompletedTodos object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [];

    return result;
  }

  @override
  RemoveCompletedTodos deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new RemoveCompletedTodosBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class RemoveCompletedTodos
// **************************************************************************

class _$RemoveCompletedTodos extends RemoveCompletedTodos {
  factory _$RemoveCompletedTodos([updates(RemoveCompletedTodosBuilder b)]) =>
      (new RemoveCompletedTodosBuilder()..update(updates)).build();

  _$RemoveCompletedTodos._() : super._();

  @override
  RemoveCompletedTodos rebuild(updates(RemoveCompletedTodosBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  RemoveCompletedTodosBuilder toBuilder() =>
      new RemoveCompletedTodosBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! RemoveCompletedTodos) return false;
    return true;
  }

  @override
  int get hashCode {
    return 546285367;
  }

  @override
  String toString() {
    return 'RemoveCompletedTodos {}';
  }
}

class RemoveCompletedTodosBuilder
    implements Builder<RemoveCompletedTodos, RemoveCompletedTodosBuilder> {
  RemoveCompletedTodos _$v;

  RemoveCompletedTodosBuilder();

  RemoveCompletedTodosBuilder get _$this {
    if (_$v != null) {
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RemoveCompletedTodos other) {
    _$v = other;
  }

  @override
  void update(updates(RemoveCompletedTodosBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  RemoveCompletedTodos build() {
    final result = _$v ?? new _$RemoveCompletedTodos._();
    replace(result);
    return result;
  }
}
