library undo_remove;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/models/todo.dart';

part 'undo_remove.g.dart';

abstract class UndoRemoveTodo
    implements Built<UndoRemoveTodo, UndoRemoveTodoBuilder>, AppAction {
  static Serializer<UndoRemoveTodo> get serializer =>
      _$undoRemoveTodoSerializer;

  Todo get todo;

  UndoRemoveTodo._();

  factory UndoRemoveTodo([updates(UndoRemoveTodoBuilder b)]) = _$UndoRemoveTodo;
}
