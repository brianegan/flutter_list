library toggle_todo_complete;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'toggle_todo_complete.g.dart';

abstract class ToggleTodoComplete
    implements Built<ToggleTodoComplete, ToggleTodoCompleteBuilder>, AppAction {
  static Serializer<ToggleTodoComplete> get serializer =>
      _$toggleTodoCompleteSerializer;

  String get id;

  bool get isComplete;

  ToggleTodoComplete._();

  factory ToggleTodoComplete([updates(ToggleTodoCompleteBuilder b)]) =
      _$ToggleTodoComplete;
}
