library edit_todo_task;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'edit_todo_task.g.dart';

abstract class EditTodoTask
    implements Built<EditTodoTask, EditTodoTaskBuilder>, AppAction {
  static Serializer<EditTodoTask> get serializer => _$editTodoTaskSerializer;

  String get id;

  String get task;

  EditTodoTask._();

  factory EditTodoTask([updates(EditTodoTaskBuilder b)]) = _$EditTodoTask;
}
