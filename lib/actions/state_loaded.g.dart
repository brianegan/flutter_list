// GENERATED CODE - DO NOT MODIFY BY HAND

part of state_loaded;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library state_loaded
// **************************************************************************

Serializer<StateLoaded> _$stateLoadedSerializer = new _$StateLoadedSerializer();

class _$StateLoadedSerializer implements StructuredSerializer<StateLoaded> {
  @override
  final Iterable<Type> types = const [StateLoaded, _$StateLoaded];
  @override
  final String wireName = 'StateLoaded';

  @override
  Iterable serialize(Serializers serializers, StateLoaded object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(AppState)),
    ];

    return result;
  }

  @override
  StateLoaded deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new StateLoadedBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'state':
            result.state.replace(serializers.deserialize(value,
                specifiedType: const FullType(AppState)));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class StateLoaded
// **************************************************************************

class _$StateLoaded extends StateLoaded {
  @override
  final AppState state;

  factory _$StateLoaded([updates(StateLoadedBuilder b)]) =>
      (new StateLoadedBuilder()..update(updates)).build();

  _$StateLoaded._({this.state}) : super._() {
    if (state == null) throw new ArgumentError.notNull('state');
  }

  @override
  StateLoaded rebuild(updates(StateLoadedBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  StateLoadedBuilder toBuilder() => new StateLoadedBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! StateLoaded) return false;
    return state == other.state;
  }

  @override
  int get hashCode {
    return $jf($jc(0, state.hashCode));
  }

  @override
  String toString() {
    return 'StateLoaded {'
        'state=${state.toString()},\n'
        '}';
  }
}

class StateLoadedBuilder implements Builder<StateLoaded, StateLoadedBuilder> {
  StateLoaded _$v;

  AppStateBuilder _state;
  AppStateBuilder get state => _$this._state ??= new AppStateBuilder();
  set state(AppStateBuilder state) => _$this._state = state;

  StateLoadedBuilder();

  StateLoadedBuilder get _$this {
    if (_$v != null) {
      _state = _$v.state?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StateLoaded other) {
    _$v = other;
  }

  @override
  void update(updates(StateLoadedBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  StateLoaded build() {
    final result = _$v ?? new _$StateLoaded._(state: state?.build());
    replace(result);
    return result;
  }
}
