library remove_completed;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/app_action.dart';

part 'remove_completed.g.dart';

abstract class RemoveCompletedTodos
    implements
        Built<RemoveCompletedTodos, RemoveCompletedTodosBuilder>,
        AppAction {
  static Serializer<RemoveCompletedTodos> get serializer =>
      _$removeCompletedTodosSerializer;

  RemoveCompletedTodos._();

  factory RemoveCompletedTodos([updates(RemoveCompletedTodosBuilder b)]) =
      _$RemoveCompletedTodos;
}
