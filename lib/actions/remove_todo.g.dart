// GENERATED CODE - DO NOT MODIFY BY HAND

part of remove_todo;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library remove_todo
// **************************************************************************

Serializer<RemoveTodo> _$removeTodoSerializer = new _$RemoveTodoSerializer();

class _$RemoveTodoSerializer implements StructuredSerializer<RemoveTodo> {
  @override
  final Iterable<Type> types = const [RemoveTodo, _$RemoveTodo];
  @override
  final String wireName = 'RemoveTodo';

  @override
  Iterable serialize(Serializers serializers, RemoveTodo object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  RemoveTodo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new RemoveTodoBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'id':
            result.id = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class RemoveTodo
// **************************************************************************

class _$RemoveTodo extends RemoveTodo {
  @override
  final String id;

  factory _$RemoveTodo([updates(RemoveTodoBuilder b)]) =>
      (new RemoveTodoBuilder()..update(updates)).build();

  _$RemoveTodo._({this.id}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
  }

  @override
  RemoveTodo rebuild(updates(RemoveTodoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  RemoveTodoBuilder toBuilder() => new RemoveTodoBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! RemoveTodo) return false;
    return id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(0, id.hashCode));
  }

  @override
  String toString() {
    return 'RemoveTodo {'
        'id=${id.toString()},\n'
        '}';
  }
}

class RemoveTodoBuilder implements Builder<RemoveTodo, RemoveTodoBuilder> {
  RemoveTodo _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  RemoveTodoBuilder();

  RemoveTodoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RemoveTodo other) {
    _$v = other;
  }

  @override
  void update(updates(RemoveTodoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  RemoveTodo build() {
    final result = _$v ?? new _$RemoveTodo._(id: id);
    replace(result);
    return result;
  }
}
