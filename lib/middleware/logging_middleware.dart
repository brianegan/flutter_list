import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:redux/redux.dart';

class LoggingMiddleware implements Middleware<AppState, AppAction> {
  call(store, action, next) {
    print('${new DateTime.now()}: $action');

    next(action);
  }
}
