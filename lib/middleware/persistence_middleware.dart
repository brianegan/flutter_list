import 'package:flutter_list/actions/app_action.dart';
import 'package:flutter_list/actions/init.dart';
import 'package:flutter_list/actions/state_loaded.dart';
import 'package:flutter_list/data/app_state_repository.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:redux/redux.dart';

// Responsible for restoring the AppState when the application is initialized,
// and persisting the latest AppState every time it changes.
class PersistenceMiddleware implements Middleware<AppState, AppAction> {
  final AppStateRepository repository;

  PersistenceMiddleware({AppStateRepository repository})
      : this.repository = repository ?? new AppStateRepository();

  call(store, action, next) async {
    next(action);

    if (action is Init) {
      var state = await repository.loadState();

      store.dispatch(new StateLoaded((b) => b..state = state.toBuilder()));
    } else if (!(action is StateLoaded)) {
      repository.saveState(store.state);
    }
  }
}
