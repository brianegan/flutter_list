// GENERATED CODE - DO NOT MODIFY BY HAND

part of todo;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library todo
// **************************************************************************

Serializer<Todo> _$todoSerializer = new _$TodoSerializer();

class _$TodoSerializer implements StructuredSerializer<Todo> {
  @override
  final Iterable<Type> types = const [Todo, _$Todo];
  @override
  final String wireName = 'Todo';

  @override
  Iterable serialize(Serializers serializers, Todo object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'isComplete',
      serializers.serialize(object.isComplete,
          specifiedType: const FullType(bool)),
      'isFavorite',
      serializers.serialize(object.isFavorite,
          specifiedType: const FullType(bool)),
      'task',
      serializers.serialize(object.task, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Todo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new TodoBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'id':
            result.id = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
          case 'isComplete':
            result.isComplete = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
          case 'isFavorite':
            result.isFavorite = serializers.deserialize(value,
                specifiedType: const FullType(bool));
            break;
          case 'task':
            result.task = serializers.deserialize(value,
                specifiedType: const FullType(String));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class Todo
// **************************************************************************

class _$Todo extends Todo {
  @override
  final String id;
  @override
  final bool isComplete;
  @override
  final bool isFavorite;
  @override
  final String task;

  factory _$Todo([updates(TodoBuilder b)]) =>
      (new TodoBuilder()..update(updates)).build();

  _$Todo._({this.id, this.isComplete, this.isFavorite, this.task}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (isComplete == null) throw new ArgumentError.notNull('isComplete');
    if (isFavorite == null) throw new ArgumentError.notNull('isFavorite');
    if (task == null) throw new ArgumentError.notNull('task');
  }

  @override
  Todo rebuild(updates(TodoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TodoBuilder toBuilder() => new TodoBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! Todo) return false;
    return id == other.id &&
        isComplete == other.isComplete &&
        isFavorite == other.isFavorite &&
        task == other.task;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), isComplete.hashCode), isFavorite.hashCode),
        task.hashCode));
  }

  @override
  String toString() {
    return 'Todo {'
        'id=${id.toString()},\n'
        'isComplete=${isComplete.toString()},\n'
        'isFavorite=${isFavorite.toString()},\n'
        'task=${task.toString()},\n'
        '}';
  }
}

class TodoBuilder implements Builder<Todo, TodoBuilder> {
  Todo _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _isComplete;
  bool get isComplete => _$this._isComplete;
  set isComplete(bool isComplete) => _$this._isComplete = isComplete;

  bool _isFavorite;
  bool get isFavorite => _$this._isFavorite;
  set isFavorite(bool isFavorite) => _$this._isFavorite = isFavorite;

  String _task;
  String get task => _$this._task;
  set task(String task) => _$this._task = task;

  TodoBuilder();

  TodoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _isComplete = _$v.isComplete;
      _isFavorite = _$v.isFavorite;
      _task = _$v.task;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Todo other) {
    _$v = other;
  }

  @override
  void update(updates(TodoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  Todo build() {
    final result = _$v ??
        new _$Todo._(
            id: id, isComplete: isComplete, isFavorite: isFavorite, task: task);
    replace(result);
    return result;
  }
}
