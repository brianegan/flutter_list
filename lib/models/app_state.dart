library app_state;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/models/todo.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  static Serializer<AppState> get serializer => _$appStateSerializer;

  BuiltList<Todo> get todos;

  AppState._();

  factory AppState([updates(AppStateBuilder b)]) = _$AppState;

  factory AppState.of(Iterable<Todo> todos) =>
      new AppState((b) => b..todos = new ListBuilder<Todo>(todos));

  factory AppState.empty() =>
      new AppState((b) => b..todos = new ListBuilder<Todo>());
}
