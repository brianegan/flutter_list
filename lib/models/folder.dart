library folder;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'folder.g.dart';

abstract class Folder implements Built<Folder, FolderBuilder> {
  String get id;
  BuiltList<String> get todoListIds;
  String get name;

  Folder._();

  factory Folder([updates(FolderBuilder b)]) = _$Folder;
}
