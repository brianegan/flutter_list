library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_list/actions/add_folder.dart';
import 'package:flutter_list/actions/add_todo.dart';
import 'package:flutter_list/actions/add_todo_list.dart';
import 'package:flutter_list/actions/edit_todo_task.dart';
import 'package:flutter_list/actions/init.dart';
import 'package:flutter_list/actions/remove_completed.dart';
import 'package:flutter_list/actions/remove_todo.dart';
import 'package:flutter_list/actions/state_load_error.dart';
import 'package:flutter_list/actions/state_loaded.dart';
import 'package:flutter_list/actions/toggle_todo_complete.dart';
import 'package:flutter_list/actions/toggle_todo_favorite.dart';
import 'package:flutter_list/actions/undo_remove.dart';
import 'package:flutter_list/models/app_state.dart';
import 'package:flutter_list/models/todo.dart';

part 'serializers.g.dart';

Serializers serializers = _$serializers;
