// GENERATED CODE - DO NOT MODIFY BY HAND

part of app_state;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library app_state
// **************************************************************************

Serializer<AppState> _$appStateSerializer = new _$AppStateSerializer();

class _$AppStateSerializer implements StructuredSerializer<AppState> {
  @override
  final Iterable<Type> types = const [AppState, _$AppState];
  @override
  final String wireName = 'AppState';

  @override
  Iterable serialize(Serializers serializers, AppState object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = [
      'todos',
      serializers.serialize(object.todos,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Todo)])),
    ];

    return result;
  }

  @override
  AppState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new AppStateBuilder();

    var key;
    var value;
    var expectingKey = true;
    for (final item in serialized) {
      if (expectingKey) {
        key = item;
        expectingKey = false;
      } else {
        value = item;
        expectingKey = true;

        switch (key as String) {
          case 'todos':
            result.todos.replace(serializers.deserialize(value,
                specifiedType:
                    const FullType(BuiltList, const [const FullType(Todo)])));
            break;
        }
      }
    }

    return result.build();
  }
}

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class AppState
// **************************************************************************

class _$AppState extends AppState {
  @override
  final BuiltList<Todo> todos;

  factory _$AppState([updates(AppStateBuilder b)]) =>
      (new AppStateBuilder()..update(updates)).build();

  _$AppState._({this.todos}) : super._() {
    if (todos == null) throw new ArgumentError.notNull('todos');
  }

  @override
  AppState rebuild(updates(AppStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! AppState) return false;
    return todos == other.todos;
  }

  @override
  int get hashCode {
    return $jf($jc(0, todos.hashCode));
  }

  @override
  String toString() {
    return 'AppState {'
        'todos=${todos.toString()},\n'
        '}';
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  AppState _$v;

  ListBuilder<Todo> _todos;
  ListBuilder<Todo> get todos => _$this._todos ??= new ListBuilder<Todo>();
  set todos(ListBuilder<Todo> todos) => _$this._todos = todos;

  AppStateBuilder();

  AppStateBuilder get _$this {
    if (_$v != null) {
      _todos = _$v.todos?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    _$v = other;
  }

  @override
  void update(updates(AppStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  AppState build() {
    final result = _$v ?? new _$AppState._(todos: todos?.build());
    replace(result);
    return result;
  }
}
