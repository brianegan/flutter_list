library todo;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:uuid/uuid.dart';

part 'todo.g.dart';

abstract class Todo implements Built<Todo, TodoBuilder> {
  static Serializer<Todo> get serializer => _$todoSerializer;

  String get id;

  bool get isComplete;

  bool get isFavorite;

  String get task;

  Todo._();

  factory Todo([updates(TodoBuilder b)]) = _$Todo;

  factory Todo.from(String task) => new Todo((b) => b
    ..task = task
    ..isComplete = false
    ..isFavorite = false
    ..id = new Uuid().v4());
}
