library todo_list;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'todo_list.g.dart';

abstract class TodoList implements Built<TodoList, TodoListBuilder> {
  String get id;
  BuiltList<String> get todoIds;
  String get name;

  TodoList._();

  factory TodoList([updates(TodoListBuilder b)]) = _$TodoList;
}
