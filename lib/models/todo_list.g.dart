// GENERATED CODE - DO NOT MODIFY BY HAND

part of todo_list;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class TodoList
// **************************************************************************

class _$TodoList extends TodoList {
  @override
  final String id;
  @override
  final BuiltList<String> todoIds;
  @override
  final String name;

  factory _$TodoList([updates(TodoListBuilder b)]) =>
      (new TodoListBuilder()..update(updates)).build();

  _$TodoList._({this.id, this.todoIds, this.name}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (todoIds == null) throw new ArgumentError.notNull('todoIds');
    if (name == null) throw new ArgumentError.notNull('name');
  }

  @override
  TodoList rebuild(updates(TodoListBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TodoListBuilder toBuilder() => new TodoListBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! TodoList) return false;
    return id == other.id && todoIds == other.todoIds && name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), todoIds.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return 'TodoList {'
        'id=${id.toString()},\n'
        'todoIds=${todoIds.toString()},\n'
        'name=${name.toString()},\n'
        '}';
  }
}

class TodoListBuilder implements Builder<TodoList, TodoListBuilder> {
  TodoList _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ListBuilder<String> _todoIds;
  ListBuilder<String> get todoIds =>
      _$this._todoIds ??= new ListBuilder<String>();
  set todoIds(ListBuilder<String> todoIds) => _$this._todoIds = todoIds;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  TodoListBuilder();

  TodoListBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _todoIds = _$v.todoIds?.toBuilder();
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TodoList other) {
    _$v = other;
  }

  @override
  void update(updates(TodoListBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  TodoList build() {
    final result =
        _$v ?? new _$TodoList._(id: id, todoIds: todoIds?.build(), name: name);
    replace(result);
    return result;
  }
}
