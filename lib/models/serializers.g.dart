// GENERATED CODE - DO NOT MODIFY BY HAND

part of serializers;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: library serializers
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AddFolder.serializer)
      ..add(Todo.serializer)
      ..add(StateLoadError.serializer)
      ..add(AddTodoList.serializer)
      ..add(UndoRemoveTodo.serializer)
      ..add(AddTodo.serializer)
      ..add(RemoveCompletedTodos.serializer)
      ..add(RemoveTodo.serializer)
      ..add(Init.serializer)
      ..add(EditTodoTask.serializer)
      ..add(ToggleTodoFavorite.serializer)
      ..add(ToggleTodoComplete.serializer)
      ..add(StateLoaded.serializer)
      ..add(AppState.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Todo)]),
          () => new ListBuilder<Todo>()))
    .build();
