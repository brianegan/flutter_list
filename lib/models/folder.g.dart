// GENERATED CODE - DO NOT MODIFY BY HAND

part of folder;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class Folder
// **************************************************************************

class _$Folder extends Folder {
  @override
  final String id;
  @override
  final BuiltList<String> todoListIds;
  @override
  final String name;

  factory _$Folder([updates(FolderBuilder b)]) =>
      (new FolderBuilder()..update(updates)).build();

  _$Folder._({this.id, this.todoListIds, this.name}) : super._() {
    if (id == null) throw new ArgumentError.notNull('id');
    if (todoListIds == null) throw new ArgumentError.notNull('todoListIds');
    if (name == null) throw new ArgumentError.notNull('name');
  }

  @override
  Folder rebuild(updates(FolderBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  FolderBuilder toBuilder() => new FolderBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! Folder) return false;
    return id == other.id &&
        todoListIds == other.todoListIds &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, id.hashCode), todoListIds.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return 'Folder {'
        'id=${id.toString()},\n'
        'todoListIds=${todoListIds.toString()},\n'
        'name=${name.toString()},\n'
        '}';
  }
}

class FolderBuilder implements Builder<Folder, FolderBuilder> {
  Folder _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ListBuilder<String> _todoListIds;
  ListBuilder<String> get todoListIds =>
      _$this._todoListIds ??= new ListBuilder<String>();
  set todoListIds(ListBuilder<String> todoListIds) =>
      _$this._todoListIds = todoListIds;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  FolderBuilder();

  FolderBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _todoListIds = _$v.todoListIds?.toBuilder();
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Folder other) {
    _$v = other;
  }

  @override
  void update(updates(FolderBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  Folder build() {
    final result = _$v ??
        new _$Folder._(id: id, todoListIds: todoListIds?.build(), name: name);
    replace(result);
    return result;
  }
}
